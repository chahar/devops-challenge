# Devops-challenge



**TODO** 
- [ ] Manual Jobs for Terragrunt apply. (We should first validate what's in the plan and then only trigger apply if required, 
Currently I have commented manual triggers to automate the whole pipeline)
- [ ] Move helm deployment to a separate repository.
- [ ] Move variables from terraform variables file to Terragrunt.
- [ ] Add Helm Diff job
- [ ] logging, monitoring and alerting.
- [ ] Enable SELinux. (Build a secure ami image and then use it as base ami)
- [ ] Redirect to HTTPS, when there will be a domain.
- [ ] Add Ingress resource instead of using Service as LoadBalancer.
