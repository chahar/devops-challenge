include {
  path = find_in_parent_folders()
}


terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform//eks"
}

# Input values

inputs = {
  vpc_name = "sandbox"
}


