remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket         = local.bucket
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.region
    encrypt        = true
    dynamodb_table = "lock-table"
  }
}

locals {
  bucket = "devops-task-bucket-tf-state-5"
  region = "us-east-1"
}

inputs = {
  region = local.region
  default_tags = {
    environment = "sandbox"
  }
}