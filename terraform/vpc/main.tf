provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 2.47"

  name                   = var.vpc_name
  cidr                   = var.cidr
  azs                    = var.availability_zones
  private_subnets        = var.private_subnets
  public_subnets         = var.public_subnets
  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az
  enable_dns_hostnames   = var.enable_dns_hostnames
}

resource "aws_iam_instance_profile" "ec2" {
  name = "ec2"
  role = aws_iam_role.role.name
}

resource "aws_iam_role" "role" {
  name = "ec2"

  # Terraform's "jsonencode" function converts a Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "s3" {
  name = "ec2_s3_access"
  role = aws_iam_role.role.id

  #tfsec:ignore:AWS099
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

module "private_ec2" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group
  name = "asg-private"

  min_size                  = 0
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.private_subnets

  # Launch template
  lt_name                  = "private-asg"
  user_data_base64         = data.template_cloudinit_config.private.rendered
  security_groups          = [aws_security_group.ec2_private.id]
  iam_instance_profile_arn = aws_iam_instance_profile.ec2.arn
  description              = "Launch template for EC2 instances in private subnets"
  update_default_version   = true
  root_block_device = [{
    encrypted = true
  }]

  use_lt    = true
  create_lt = true

  image_id          = var.image_id
  instance_type     = var.instance_type
  target_group_arns = module.nlb.target_group_arns
  ebs_optimized     = true
  enable_monitoring = true

  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/xvda"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 20
        volume_type           = "gp2"
      }
      }, {
      device_name = "/dev/sda1"
      no_device   = 1
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 30
        volume_type           = "gp2"
      }
    }
  ]

  tags = [
    {
      key                 = "environment"
      value               = "sandbox"
      propagate_at_launch = true
    },
    {
      key                 = "Category"
      value               = "private instance"
      propagate_at_launch = true
    },
  ]
}

module "public_ec2" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group
  name = "asg-public"

  min_size                  = 0
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.public_subnets

  # Launch template
  lt_name                     = "public-asg"
  user_data_base64            = data.template_cloudinit_config.public.rendered
  security_groups             = [aws_security_group.ec2_public.id]
  description                 = "Launch template for EC2 instances in public subnets"
  associate_public_ip_address = true #tfsec:ignore:AWS012 ignore warning as this config is valid
  update_default_version      = true
  root_block_device = [{
    encrypted = true
  }]

  use_lt    = true
  create_lt = true

  image_id          = var.image_id
  instance_type     = var.instance_type
  ebs_optimized     = true
  enable_monitoring = true

  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/xvda"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 20
        volume_type           = "gp2"
      }
      }, {
      device_name = "/dev/sda1"
      no_device   = 1
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 30
        volume_type           = "gp2"
      }
    }
  ]

  tags = [
    {
      key                 = "environment"
      value               = "sandbox"
      propagate_at_launch = true
    },
    {
      key                 = "Category"
      value               = "public instance"
      propagate_at_launch = true
    },
  ]
}

#tfsec:ignore:AWS005 aws-elbv2-alb-not-public allow public ip
module "nlb" {
  source             = "terraform-aws-modules/alb/aws"
  version            = "~> 6.0"
  name               = "sandbox-nlb"
  load_balancer_type = "network"
  vpc_id             = module.vpc.vpc_id
  internal           = false
  subnets            = module.vpc.public_subnets
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    },
    {
      port               = 443
      protocol           = "TCP"
      target_group_index = 1
    },
  ]
  target_groups = [
    {
      name_prefix        = "tg1-"
      backend_protocol   = "TCP"
      backend_port       = 80
      target_type        = "instance"
      preserve_client_ip = true
    },
    {
      name_prefix      = "tg2-"
      backend_protocol = "TCP"
      backend_port     = 443
      target_type      = "instance"
    },
  ]
}

data "template_file" "private" {
  template = file("${path.module}/scripts/nginx_setup.tmpl")

  vars = {
    SERVER_TYPE = "local" # To get Private IP of host
    SERVER_NAME = var.private_server_name
  }
}

data "template_file" "public" {
  template = file("${path.module}/scripts/nginx_setup.tmpl")

  vars = {
    SERVER_TYPE = "public" #To get Public IP of host
    SERVER_NAME = var.public_server_name
  }
}

data "template_cloudinit_config" "private" {
  gzip          = true
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    filename     = "nginx.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.private.rendered
  }
}

data "template_cloudinit_config" "public" {
  gzip          = true
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    filename     = "nginx.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.public.rendered
  }
}