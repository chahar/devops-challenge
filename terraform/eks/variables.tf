# Variables are defined in Terragrunt

variable "vpc_name" {
  description = "name of VPC"
  type        = string
  default     = "sandbox"
}

variable "cluster_name" {
  description = "Name of EKS cluster"
  type        = string
  default     = "sandbox"
}

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "default_tags" {
  type        = map(string)
  description = "default tags"
  default = {
    environment = "sandbox"
  }
}

variable "vpc_filter_name" {
  type    = string
  default = "tag:environment"
}

variable "vpc_filter_values" {
  type    = list(string)
  default = ["sandbox"]
}

variable "subnets_filter_name" {
  type    = string
  default = "tag:Name"
}

variable "subnets_filter_values" {
  type    = list(string)
  default = ["*private*"]
}

variable "asg_desired_capacity" {
  type    = number
  default = 1
}

variable "instance_type_worker_1" {
  type    = string
  default = "t3.small"
}

variable "instance_type_worker_2" {
  type    = string
  default = "t3.medium"
}