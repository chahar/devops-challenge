provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}

data "aws_vpc" "selected" {
  filter {
    name   = var.vpc_filter_name
    values = var.vpc_filter_values
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = var.subnets_filter_name
    values = var.subnets_filter_values
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "17.20.0"
  cluster_name    = var.cluster_name
  cluster_version = "1.21"
  subnets         = data.aws_subnet_ids.selected.ids
  vpc_id          = data.aws_vpc.selected.id

  worker_groups_launch_template = [
    {
      name                 = "worker-group-1"
      instance_type        = var.instance_type_worker_1
      asg_desired_capacity = var.asg_desired_capacity
      public_ip            = false
      tags = [{
        key                 = "ExtraTag"
        value               = "TagValue"
        propagate_at_launch = true
      }]
    },
    {
      name                 = "worker-group-2"
      instance_type        = var.instance_type_worker_2
      asg_desired_capacity = var.asg_desired_capacity
      public_ip            = false
    },
  ]
}
